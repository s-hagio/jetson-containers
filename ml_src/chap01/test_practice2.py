import pytest
import practice2


class TestPra2(object):
    def setup_method(self):
        self.hello = practice2.Hello()
        self.names = ["田中", "佐藤", "山田", "高橋"]

    def test_ret_hellos(self):
        assert self.hello.ret_hellos("田中") == "田中さん、こんにちは"
        with pytest.raises(ValueError):
            self.hello.ret_hellos(32)
