import pytest  # noqa: errors
import practice


def test_ret_info_myself() -> bool:
    f = practice.ret_info_myself
    assert f("萩尾", "真二", "31") == "私は萩尾真二です。31才です。"
    assert f("萩尾", "真二", 31) == "私は萩尾真二です。31才です。"
